<tr class="pricing-item row-<?php echo $row; ?>" rel="<?php echo $row; ?>">
    <td class="sort"></td>

    <td class="">
        <input type="text" value="<?php echo isset ($pricing_item['price-from']) ? $pricing_item['price-from'] : "";?>" placeholder="0" name="price-from[<?php echo $row; ?>]" class="price-from-<?php echo $row; ?>">
    </td>

    <td class="">
        <input type="text" value="<?php echo isset ($pricing_item['price-to']) ? $pricing_item['price-to'] : "";?>" placeholder="0" name="price-to[<?php echo $row; ?>]" class="price-to-<?php echo $row; ?>">
    </td>

    <td class="">
        <input type="text" value="<?php echo isset ($pricing_item['weight-from']) ? $pricing_item['weight-from'] : "";?>" placeholder="0" name="weight-from[<?php echo $row; ?>]" class="weight-from-<?php echo $row; ?>">
    </td>

    <td class="">
        <input type="text" value="<?php echo isset ($pricing_item['weight-to']) ? $pricing_item['weight-to'] : "";?>" placeholder="0" name="weight-to[<?php echo $row; ?>]" class="weight-to-<?php echo $row; ?>">
    </td>

    <td class="">
        <select name="shipping-class[<?php echo $row; ?>][]" class="multiple-select shipping-class-<?php echo $row; ?>" multiple="multiple">

            <?php
                $selected = isset ($pricing_item['shipping-class']) ? $pricing_item['shipping-class'] : false;
                echo '<option value="none" ';
                if ((is_array($selected) && in_array('none', $selected)) || ($selected === false)) {
                    echo "selected";
                }
                echo '>'.__('No shipping class', 'envoimoinscher').'</option>';
                foreach ($shipping_classes as $class) {
                    echo '<option value="'.$class->slug.'" ';
                    if ((is_array($selected) && in_array($class->slug, $selected)) || $selected === false) {
                        echo "selected";
                    }
                    echo '>'.$class->name.' ('.$class->slug.')</option>';
                }
            ?>
        </select>
    </td>

    <td class="">
        <?php if (count($zones_countries) > 0) { ?>
            <select name="zones[<?php echo $row; ?>][]" class="multiple-select zones-<?php echo $row; ?>" multiple="multiple">
                <?php
                    $selected = isset ($pricing_item['zones']) ? $pricing_item['zones'] : false;
                    if (envoimoinscher_model::is_zones_enabled()) {
                        foreach ($zones_countries as $zone) {
                            $zone_id = isset($zone['zone_id']) ? $zone['zone_id']:$zone['id'];
                            echo '<option value="'.$zone_id.'" ';
                            if ((is_array($selected) && in_array((string)$zone_id, $selected, true)) || $selected === false){
                                echo "selected";
                            }
                            echo '>'.$zone['zone_name'].'</option>';
                        }
                    }
                    else {
                        foreach ($zones_countries as $iso => $label) {
                            echo '<option value="'.$iso.'" ';
                            if ((is_array($selected) && in_array((string)$iso, $selected,true)) || $selected === false){
                                echo "selected";
                            }
                            echo '>'.$label.'</option>';
                        }
                    }
                ?>
            </select>
        <?php } else {
            echo '<div class="err">';
            // est-ce qu'il est possible de n'avoir aucun pays ?
            echo envoimoinscher_model::is_zones_enabled()?__('This service is not associated with any zone', 'envoimoinscher'):__('Please activate at least one country', 'envoimoinscher');
            echo '</div><input type="hidden" name="zones['.$row.'][]" />';
        } ?>
    </td>

    <td class="handling-fees">
        <input type="text" value="<?php echo isset ($pricing_item['handling-fees']) ? $pricing_item['handling-fees'] : "";?>" placeholder="0" name="handling-fees[<?php echo $row; ?>]" class="handling-fees-<?php echo $row; ?>"
         <?php if (!isset ($pricing_item['pricing']) || (isset ($pricing_item['pricing']) && ($pricing_item['pricing'] == "free" || $pricing_item['pricing'] == "deactivate"))) echo "disabled"; ?>
        >
    </td>

    <td class="">
        <select name="pricing[<?php echo $row; ?>]" class="single-select pricing pricing-<?php echo $row; ?>">
            <?php
                $default_value = isset ($pricing_item['pricing']) ? $pricing_item['pricing'] : "live";
            ?>
            <option value="live" <?php if ($default_value == "live") echo "selected"; ?>><?php _e('Boxtal quote', 'envoimoinscher'); ?></option>
            <option value="rate" <?php if ($default_value == "rate") echo "selected"; ?>><?php _e('Flat rate', 'envoimoinscher'); ?></option>
            <option value="free" <?php if ($default_value == "free") echo "selected"; ?>><?php _e('Free', 'envoimoinscher'); ?></option>
            <option value="deactivate" <?php if ($default_value == "deactivate") echo "selected"; ?>><?php _e('Deactivated', 'envoimoinscher'); ?></option>
        </select>
    </td>

    <td class="flat-rate">
        <input type="text" value="<?php echo isset ($pricing_item['flat-rate']) ? $pricing_item['flat-rate'] : "";?>" placeholder="0" name="flat-rate[<?php echo $row; ?>]" class="flat-rate-<?php echo $row; ?>"
        <?php if (!isset ($pricing_item['pricing']) || (isset ($pricing_item['pricing']) && $pricing_item['pricing'] != "rate")) echo "disabled"; ?>
        >
    </td>
</tr>
