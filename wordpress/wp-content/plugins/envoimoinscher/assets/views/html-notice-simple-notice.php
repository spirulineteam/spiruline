<?php
/**
 * Admin View: Simple Notices
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="<?php esc_attr_e($notice['status']); ?> emc-notice">
    <?php echo vsprintf(__( $notice['message'], 'envoimoinscher' ), $notice['placeholders']); ?>
</div>