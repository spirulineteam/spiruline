<?php
/**
 * Admin View: News
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="emc-notice <?php esc_attr_e($notice['status']); ?>">
    <?php
        if ($logo) {
            echo '<img src="'.plugins_url( $img , EMC_PLUGIN_FILE ).'" alt="'.__('Boxtal', 'envoimoinscher').'" class="mini-logo" />';
        }
    ?>
    <b><?php echo $notice['message_short']; ?></b> : <?php echo $notice['message']; ?>
    <br/>
    <a class="button-secondary hide-emc-notice mt5" rel="<?php echo md5($notice['status'].$notice['message'].$notice['message_short']); ?>">
    <?php _e( 'Hide this notice', 'envoimoinscher' ); ?>
    </a>
</div>