<?php
/**
 * Admin View: Notice - Update
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="notice emc-notice update">
    <p>
        <?php
            if ($logo) {
                echo '<img src="'.plugins_url( $img , EMC_PLUGIN_FILE ).'" alt="'.__('Boxtal', 'envoimoinscher').'" class="mini-logo" />';
            }
        ?>
        <strong><?php _e( 'Boxtal Data Update', 'envoimoinscher' ); ?></strong> <?php _e( 'We need to update your store\'s database to the latest version.', 'envoimoinscher' ); ?>
    </p>
    <p>
        <a class="button-secondary emc-update-now" href="<?php echo esc_url( add_query_arg( 'do_update_envoimoinscher', 'true', admin_url( 'admin.php?page=envoimoinscher-settings' ) ) ); ?>">
        <?php _e( 'Run the updater', 'envoimoinscher' ); ?>
        </a>
    </p>
</div>
<script type="text/javascript">
	jQuery( '.emc-update-now' ).click( 'click', function() {
		return window.confirm( '<?php echo esc_js( __( 'It is strongly recommended that you backup your database before proceeding. Are you sure you wish to run the updater now?', 'envoimoinscher' ) ); ?>' );
	});
</script>