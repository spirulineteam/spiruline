<?php
/**
 * Admin View: mass order notice
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

echo '<input type="hidden" name="massOrderIds" value="'.implode(',', $notice['order_ids']).'" />';
foreach ($notice['order_ids'] as $order_id) {
    $multiparcels = get_post_meta($order_id, '_multiparcels', true);
    $suborder_keys = array();
    foreach ($multiparcels as $multiparcel_info) {
        array_push($suborder_keys, $multiparcel_info['suborder_key']);
    }
    echo '<input type="hidden" name="bulk_order_'.$order_id.'" value="'.implode(',', $suborder_keys).'" />';
}

?>
<div id="mass-order">
    <div class="ongoing emc-notice success">
        <div class="title"><?php _e('Processing orders', 'envoimoinscher'); ?> <span class="processed_id"></span></div>
        <div><?php _e('Successfully sent orders:', 'envoimoinscher'); ?> <span class="success_ids"></span></div>
        <div><?php _e('Orders not sent:', 'envoimoinscher'); ?> <span class="problem_ids"></span></div>
        <div><?php _e('Orders already sent:', 'envoimoinscher'); ?> <span class="already_ids"></span></div>
    </div>
    <div class="finished emc-notice">
        <div class="title"><?php _e('All orders processed.', 'envoimoinscher'); ?></div>
        <div><?php _e('Successfully sent orders:', 'envoimoinscher'); ?> <span class="success_ids"></span></div>
        <div><?php _e('Orders not sent:', 'envoimoinscher'); ?> <span class="problem_ids"></span></div>
        <div class="warning"><?php _e('Orders not sent may have missing parameters. You should check these orders individually.', 'envoimoinscher'); ?></div>
        <div><?php _e('Orders already sent:', 'envoimoinscher'); ?> <span class="already_ids"></span></div>
        <div class="already"><?php _e('Already sent orders cannot be re-sent.', 'envoimoinscher'); ?></div>
    </div>
</div>