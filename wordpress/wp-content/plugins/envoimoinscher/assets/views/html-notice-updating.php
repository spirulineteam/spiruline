<?php
/**
 * Admin View: Notice - Updating
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="notice emc-notice update">
    <p><strong><?php _e( 'Boxtal Data Update', 'envoimoinscher' ); ?></strong> &#8211; <?php _e( 'Your database is being updated in the background.', 'envoimoinscher' ); ?> <a href="<?php echo esc_url( add_query_arg( 'force_update_woocommerce', 'true', admin_url( 'admin.php?page=envoimoinscher-settings' ) ) ); ?>"><?php _e( 'Taking a while? Click here to run it now.', 'envoimoinscher' ); ?></a></p>
</div>