<?php
/**
 * Admin View: Notice - Updated
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="notice emc-notice update">
    <p><?php _e( 'Boxtal data update complete. Thank you for updating to the latest version!', 'envoimoinscher' ); ?></p>
    <p>
        <a class="button-secondary hide-emc-notice" rel="update">
        <?php _e( 'Hide this notice', 'envoimoinscher' ); ?>
        </a>
    </p>
</div>