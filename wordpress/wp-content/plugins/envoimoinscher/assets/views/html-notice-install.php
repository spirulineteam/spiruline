<?php
/**
 * Admin View: Notice - Install
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

?>
<div class="notice emc-notice update">
    <p><?php _e( 'Boxtal install is complete. Run the setup wizard to create a Boxtal account and/or generate your API keys.', 'envoimoinscher' ); ?></p>
    <p>
        <a href="<?php echo esc_url( admin_url( 'admin.php?page=emc-setup' ) ); ?>" class="button-primary">
        <?php _e( 'Run the Setup Wizard', 'envoimoinscher' ); ?>
        </a>
        <a class="button-secondary hide-emc-notice" rel="install">
        <?php _e( 'Hide this notice', 'envoimoinscher' ); ?>
        </a>
    </p>
</div>