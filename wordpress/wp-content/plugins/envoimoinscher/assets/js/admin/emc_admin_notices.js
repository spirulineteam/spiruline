jQuery(function ($) {
    $(document).ready(function(){
        $(document).on('click', '.hide-emc-notice', function() {
            var notice = $(this).closest('.emc-notice');
            var id = $(this).attr("rel");
            $.ajax({
                url: ajaxurl,
                method: "post",
                data: { action: 'hide_notice', security: ajax_nonce, id: id },
                dataType: "json",
                success: function(data) {
                    notice.hide();
                }
            });
        });
    });
});