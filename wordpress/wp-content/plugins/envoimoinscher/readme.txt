=== Boxtal ===
Contributors: Boxtale
Tags: delivery, carrier, shipping, pickup, doortodoor, boxtal, envoimoinscher, mondialrelay, relaiscolis, laposte, colissimo, chronopost, dhl, tnt, ups, fedex
Requires at least: 3.9
Tested up to: 4.8.0
Stable tag: 2.1.10

Manage multiple carriers using one single plugin and reduce your shipping costs without commitments or any contracts.

== Description ==

= 1. IMPROVE YOUR CONVERSION RATES =

Allow your customers to choose among the best offers available on the market: pickup (Mondial Relay, Relais Colis, ...), home delivery (Colissimo, Happy-Post, ...), express (Chronopost, FedEx, DHL Express, UPS, TNT Express, ...).

= 2. REDUCE YOUR SHIPPING COSTS =

Save on your shipping costs with the best shipping services without minimum volume commitments or any contracts.

= 3. SAVE TIME MANAGING YOUR SHIPMENTS =

Print shipping labels directly from your WordPress back-office. Track, cancel or reschedule your orders in one place. A single invoice for all shipping services.

= 4. ACCELERATE YOUR WEBSITE LAUNCH =

A single plugin to manage all your shipments. Boxtal is your only contact before, during and after delivery, whichever carrier you've chosen. Save time and focus on your business.

The plugin is available in English, French and Spanish.

== Installation ==

* **Install** and **activate** the Boxtal plugin through the "Plugins" menu in WordPress.
* On your first activation of the plugin, you will be prompted to **create your Boxtal account**.
* You can start using the plugin in test mode. When you want to switch to production mode, activate the **deferred payment option** in your Boxtal account: www.boxtal.com, "Preferences" section.

If you have any questions about shipping services or setup, please contact us directly at sales@boxtal.com.

== Screenshots ==

1. Choose the best shipping options
2. Easily set up your shipping rates
3. Provide your clients with different delivery options
4. Track shipments online and download waybills directly from your WordPress back-office

== Changelog ==

= 2.1.10 =
* fix add_rate for WC > 2.6

= 2.1.9 =
* fix use of wc deprecated functions

= 2.1.8 =
* bug fix on weight/dimensions table

= 2.1.7 =
* bug fix on tracking initialization

= 2.1.6 =
* bug fix on post meta initialization for bulk orders

= 2.1.5 =
* PHP7 bug fix
* library copyright update

= 2.1.4 =
* bug fix on notices / mass order

= 2.1.3 =
* WC 3.1 compatibility
* added spanish carriers
* bug fixes on plugin activation and front display date

= 2.1.2 =
* PHP7 bug fix

= 2.1.1 =
* Fixed bug on FO parcel point retrieval
* Added reset button for orders

= 2.1.0 =
* Text and carrier logos updates
* New upgrade system
* New notification system
* Spain based shipment support
* Revised admin options management
* Improved shipping content management
* Small bug fixes

= 2.0.8 =
* Improved relay map speed
* Fix on upgrades

= 2.0.7 =
* Bug fix on uninstall

= 2.0.6 =
* Bug fix on delivery waybills download

= 2.0.5 =
* Small fix on dropoff point initialization
* Reinitializing upgrade settings to prevent future problems

= 2.0.4 =
* Fix error display in simulator
* Small fix on order display
* Fix on log file generation directory

= 2.0.3 =
* Fix to prevent 404 on waybill download
* Fix for relay map link display in front office (plugin compatibility + rare cases)
* Hook review (plugin compatibility)
* Bug fix on new carriers activation

= 2.0.2 =
* Bug fix for other shipping methods in simulator
* Fix for plugin compatibility

= 2.0.1 =
* Bug fix for other shipping methods
* PHP version compatibility

= 2.0.0 =
* Multiple shipping addresses
* Option to split parcels on quotation if parcels are too heavy
* Possible to add weight options rows
* Improved simulator

= 1.4.6 =
* Added google API key

= 1.4.5 =
* Faster bulk shipping

= 1.4.4 =
* Improved relay point map for front office
* Included emc custom statuses in wc reports

= 1.4.3 =
* Bug fix on shipping classes

= 1.4.2 =
* Bug fix on simulator

= 1.4.1 =
* Bug fix on handling fees

= 1.4.0 =
* Implemented flexible carrier pricing option
* BO layout improvements
* Added option to enable/disable logs
* Added news updates

= 1.3.1 =
* Small fix for FO carrier logo display in different SSL settings

= 1.3.0 =
* Added API version 1.2
* Bug fix on mass orders
* Fixed multilingual fields for better plugin compatibility

= 1.2.9 =
* BO carrier description modifications
* WooCommerce < 2.5 compatibility

= 1.2.8 =
* Encoding fix

= 1.2.7 =
* Bug fix on quotation

= 1.2.6 =
* Improved settings
* Updated carriers with tracking url

= 1.2.5 =
* Fix for countries with no postal code and/or states

= 1.2.4 =
* Bug fix on tracking

= 1.2.3 =
* Bug fix on tracking
* Added relay point selection popup in BO
* Modified colissimo carrier logo

= 1.2.2 =
* Bug fix on front office single carrier display
* Modified API calls for faster response

= 1.2.1 =
* Bug fix on front office relay map (for some users)

= 1.2.0 =
* Added handling charges by carrier

= 1.1.6 =
* Added carrier logos in FO + related display option

= 1.1.5 =
* Fixed tracking: order statuses are now automatically updated

= 1.1.4 =
* Bug fix: google map display

= 1.1.3 =
* Bug fix: displayed carrier label

= 1.1.2 =
* Changed carrier naming
* Fixed long addresses bug on waybills

= 1.1.1 =
* Moved files to comply to WordPress standards
* Checked WordPress 4.3 support

= 1.1.0 =
* Added product variations support

= 1.0.5 =
* Small fix on checkout
* Updated plugin description

= 1.0.4 =
* Added multisite support

= 1.0.3 =
* Fixed WooCommerce 2.3.10 update bug

= 1.0.2 =
* Fixed WooCommerce settings bug
