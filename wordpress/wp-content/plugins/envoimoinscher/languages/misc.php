<?php
// ne sert à rien sauf à stocker des chaînes pour traduction

function dummy_function(){
	echo __('Manage multiple carriers using one single plugin and reduce your shipping costs without commitments or any contract to sign.', 'envoimoinscher');
	echo __('Suborder %1$s passed with the reference %2$s.', 'envoimoinscher');
	echo __('Suborder %s was already sent and hasn\'t been resent. You cannot resend suborders, but you can add new shipments using the "Add a shipment" button.', 'envoimoinscher');
	echo __('Suborder %1$s: %2$s.', 'envoimoinscher');
	echo __('One or more of your shipping addresses is no longer active. Please choose a new address.', 'envoimoinscher');
	echo __('Suborder %s was already sent and hasn\'t been resent. You cannot resend suborders, but you can add new shipments using the button below.', 'envoimoinscher');
	echo __('Waybills for your orders are not available yet.', 'envoimoinscher');
	echo __('Delivery waybills for your orders are not available yet.', 'envoimoinscher');
	echo __('There was a problem creating your default address.', 'envoimoinscher');
	echo __('%s is a required field.', 'envoimoinscher');
	echo __('Your settings have been saved.', 'envoimoinscher');
	echo __('An error occurred while flushing your cache.', 'envoimoinscher');
	echo __('Your cache has been successfully flushed.', 'envoimoinscher');
	echo __('Your carrier list has been successfully updated.', 'envoimoinscher');
	echo __('Please add products to cart in order to simulate rates.', 'envoimoinscher');
	echo __('Please fill in all the required * fields.', 'envoimoinscher');
	echo __('Your shipment content list has been successfully updated.', 'envoimoinscher');
}