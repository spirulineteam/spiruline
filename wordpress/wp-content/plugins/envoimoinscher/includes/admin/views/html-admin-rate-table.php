<?php
    $pricing_items = envoimoinscher_model::get_pricing($this->id);
    $zones_countries = array();
    if (envoimoinscher_model::is_zones_enabled()) {
        $zones_countries = envoimoinscher_model::get_zones($this->id);
    }
?>
<table id="emc_pricing_title" class="form-table">
    <thead>
        <th>
            <?php _e('Pricing rules', 'envoimoinscher'); ?>
            <p class="description"><?php _e("Add rules to manage front office price display. The rules will be considered from top to bottom and the first one matching conditions will be applied. If no rules are found, carrier won't be displayed.", 'envoimoinscher'); ?></p>
            <?php if (envoimoinscher_model::is_zones_enabled() && count($zones_countries) == 0) { ?>
            <p class="description error"><?php printf(__('This carrier is not associated to any of your shipping zones, you must associate it with at least one zone in order to configure your pricing rules. Configure your zones on the <a target="_blank" href="%s">Shipping Zones</a> tab.', 'envoimoinscher'), admin_url('admin.php?page=wc-settings&tab=shipping&section=')); ?></p>
            <?php } ?>
        </th>
    </thead>
</table>
<table id="emc_pricing" data-carrier="<?php echo $this->id; ?>" class="wc_input_table sortable widefat">
	<thead>
		<tr>
			<th rowspan="2" class="sort">&nbsp;</th>
			<th colspan="2" class="w22 center"><?php
                echo '<span class="mr2">'.__('Cart price', 'envoimoinscher').' ('.get_woocommerce_currency_symbol().')'.'</span>';
                $tooltip_html = __( "cart price tax excluded", 'envoimoinscher' );
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></th>
			<th colspan="2" class="w22 center"><?php echo __('Cart weight', 'envoimoinscher').' (kg)'; ?></th>
			<th rowspan="2" class="w11 center"><?php
                echo '<span class="mr2">'.__('Shipping class', 'envoimoinscher').'</span>';
                $tooltip_html = '<ul><li>'.__( "if you choose a shipping class, the rule will only apply to carts with all products belonging to the class", 'envoimoinscher' ).'</li>';
                $tooltip_html .= '<li>'.__( "Careful: newly created shipping classes won't be selected by default", 'envoimoinscher' ).'</li></ul>';
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></th>
			<th rowspan="2" class="center"><?php
                if (envoimoinscher_model::is_zones_enabled())
                {
                    echo '<span class="mr2">'.__('Shipping zone', 'envoimoinscher').'</span>';
                    $tooltip_html = __( "If you choose a shipping zone, the rule will only apply to customer addresses which belong to the zone you selected. You can set zones in the \"Shipping zones\" tab. Only shipping zones for which this carrier is activated will appear here.", 'envoimoinscher' );
                }
                else {
                    echo '<span class="mr2">'.__('Country', 'envoimoinscher').'</span>';
                    $tooltip_html = __( "If you choose a country, the rule will only apply to carts whose shipping country is the one you selected", 'envoimoinscher' );

                }
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></th>
			<th rowspan="2" class="w11 center"><?php
                echo '<span class="mr2">'.__('Handling fees', 'envoimoinscher').'</span>';
                $tooltip_html = sprintf(__( 'Fee excluding tax. Enter an amount, e.g. %s, or a percentage, e.g. 5%%. Leave blank or set to 0 to disable.', 'envoimoinscher' ), '2'.envoimoinscher_carrier::custom_get_price_decimal_separator().'50');
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></th>
			<th rowspan="2" class="w15 center"><?php
                echo '<span class="mr2">'.__('Pricing', 'envoimoinscher').'</span>';
                $tooltip_html = '<ul><li>'.__( "Boxtal quote: the shipping rate automatically calculated by Boxtal.com", 'envoimoinscher' ).'</li>';
                $tooltip_html .= '<li>'.__( "Flat rate: the shipping rate that you define", 'envoimoinscher' ).'</li></ul>';
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></th>
			<th rowspan="2" class="w11 center"><?php
                echo '<span class="mr2">'.__('Flat rate', 'envoimoinscher').' ('.get_woocommerce_currency_symbol().')'.'</span>';
                $tooltip_html = __( "flat rate before taxes", 'envoimoinscher' );
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></th>
		</tr>
        <tr>
			<th class="center"><?php _e('From', 'envoimoinscher'); ?> (≥)</th>
			<th class="center"><?php _e('To', 'envoimoinscher'); ?> (<)</th>
            <th class="center"><?php _e('From', 'envoimoinscher'); ?> (≥)</th>
			<th class="center"><?php _e('To', 'envoimoinscher'); ?> (<)</th>
		</tr>
	</thead>
	<tfoot>
		<tr>
			<th colspan="10">
				<a href="#" class="button insert"><?php _e('Add rule', 'envoimoinscher'); ?></a>
				<a href="#" class="button remove"><?php _e('Remove selected rule', 'envoimoinscher'); ?></a>
			</th>
		</tr>
	</tfoot>
	<tbody class="ui-sortable">
        <?php
            if (isset($pricing_items) && is_array($pricing_items)) {
                $i = 0;
                foreach($pricing_items as $pricing_item) {
                    EMC()->add_rate_line($this->id, false, $pricing_item, $i);
                    $i ++;
                }
            }
        ?>
    </tbody>
</table>
