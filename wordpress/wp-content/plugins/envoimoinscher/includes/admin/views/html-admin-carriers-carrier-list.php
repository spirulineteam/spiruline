<?php
/**
 * Admin View: Carriers
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

// added for popup
add_thickbox();
$lang = get_locale();
?>
		<br/>
        <a class="button-primary reload-action <?php if($initial) echo 'initial'; ?>" href="#" title="<?php echo __( 'Carrier update', 'envoimoinscher' ); ?>">
            <?php _e( 'Reload carriers from API', 'envoimoinscher' ); ?>
        </a>
        <?php
            $tooltip_html = __( 'update your carriers every month or so to ensure your carrier list is up to date', 'envoimoinscher' );
            if (function_exists('wc_help_tip')) {
                echo wc_help_tip( $tooltip_html );
            } else {
                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
            }
        ?>
		<a class="button-primary ml5 flush" href="#" title="<?php _e( 'Flush offers cache', 'envoimoinscher' ); ?>">
            <?php _e( 'Flush offers cache', 'envoimoinscher' ); ?>
        </a>
        <?php
            $tooltip_html = __( 'flush your offers cache if you change your active carriers in order for new carriers to be taken into account', 'envoimoinscher' );
            if (function_exists('wc_help_tip')) {
                echo wc_help_tip( $tooltip_html );
            } else {
                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
            }
        ?>
        <br/>
		<?php if ( $current_subtab == "advanced_carriers" ) : ?>
		
		<?php $carrier_family = 2; ?>
		<h3><?php printf( __( 'Weight/Dimensions based carriers', 'envoimoinscher' ) ); ?></h3>
		<p><?php printf( __( 'These carriers require dimensional weight. You must define dimensions per weight bracket for your parcels in tab "Weight options". Indicate dimensions that match the best your packages. Defaults dimensions grid may not be accurate and lead to wrong declaration to the carriers and complementary invoices.', 'envoimoinscher' ) ); ?></p>
		
		<?php else : ?>
		
		<?php $carrier_family = 1; ?>
		<h3><?php printf( __( 'Weight based carriers', 'envoimoinscher' ) ); ?></h3>
		<p><?php printf( __( 'These carriers allow you to send your parcel without dimensional weight. The weight is only used to calculate the shipping cost within the maximum size allowed by each carrier.', 'envoimoinscher' ) ); ?></p>	
		
		<?php endIf; ?>

		<table class="emc_carrier_list <?php echo $current_subtab; ?> wc_input_table widefat">
			<thead>
				<tr>

					<th><?php _e( 'Carrier', 'envoimoinscher' ); ?></th>
					
                    <th><?php _e( 'Service', 'envoimoinscher' ); ?></th>
                    
                    <th><?php _e( 'Zone', 'envoimoinscher' ); ?></th>
                    
                    <th><?php _e( 'Departure', 'envoimoinscher' ); ?></th>
                    
                    <th><?php _e( 'Arrival', 'envoimoinscher' ); ?></th>

					<th><?php _e( 'Delay', 'envoimoinscher' ); ?></th>

					<th><?php _e( 'Status', 'envoimoinscher' ); ?></th>
					
					<th><?php _e( 'Edition', 'envoimoinscher' ); ?></th>

				</tr>
			</thead>
			<tbody>
                <?php
                    // alphabetical sort on carrier name first
                    $carriers_alpha = array();
                    $i = 0; // index added to avoid same name indexes
                    foreach ( $carrier_list as $carrier ) {
                        $carriers_alpha[envoimoinscher_model::get_translation($carrier->srv_name, $lang, true).' '.envoimoinscher_model::get_translation($carrier->srv_name_bo, $lang, true).' '.$i] = $carrier;
                        $i++;
                    }
                    ksort($carriers_alpha);

                    // index carrier by type & remove carriers not in family (simple/advanced)
                    $carriers_by_type = array();

                    foreach ( $carriers_alpha as $carrier ) {
                        if( intval($carrier->srv_family) != $carrier_family) continue;
                        $carriers_by_type[$carrier->srv_delivery_type][] = $carrier;
                    }
                    ksort($carriers_by_type);
                    
                    // manage subtitles
                    $index = 0;
                    $title_count = array();
                    foreach ( $carriers_by_type as $type => $list ) {
                        $title_count[$type] = isset($carriers_by_type[($type-1)]) ? ($title_count[($type-1)] + count($carriers_by_type[($type-1)])) : 0;
                    }
                    
                    foreach ( $carriers_by_type as $type => $list ) {
                        foreach ( $list as $carrier ) { 
                            $carrier_code = $carrier->ope_code.'_'.$carrier->srv_code;

                            if( in_array( $carrier_code , $active_carriers) ) {
                                $carrier->status = 1;
                            }
                            else {
                                $carrier->status = 0;
                            }
                            
                            if ($type == 1 && $index == $title_count[$type]) {
                                echo '<tr><td class="delivery_type marker" colspan=8>'.__('Pickup point', 'envoimoinscher').'</td></tr>';
                            } elseif ($type == 2 && $index == $title_count[$type]) {
                                echo '<tr><td class="delivery_type home" colspan=8>'.__('On-site standard', 'envoimoinscher').'</td></tr>';
                            } elseif ($type == 3 && $index == $title_count[$type]) {
                                echo '<tr><td class="delivery_type express" colspan=8>'.__('Express', 'envoimoinscher').'</td></tr>';
                            }
                            $index++;
                        ?>
                    
                        <tr id="<?php echo $carrier_code; ?>" class="<?php echo ( 1 == $carrier->status ? 'active' : 'inactive' ); ?>">
                            <td class="operator">
                                <div class="name"><?php echo envoimoinscher_model::get_translation($carrier->srv_name, $lang, true); ?></div> 				
                            </td>
                            
                            <td class="service">
                                <div>
                                    <?php 
                                        echo '<span class="name">'.envoimoinscher_model::get_translation($carrier->srv_name_bo, $lang, true).'</span>';
                                        if (envoimoinscher_model::get_translation($carrier->srv_details, $lang, true) != '') {
                                            $details = envoimoinscher_model::get_translation($carrier->srv_details, $lang, true);
                                            $tooltip_html = '';
                                            foreach ($details as $detail) {
                                                $tooltip_html .= '<div class="carrier_tip">'.$detail.'</div>';
                                            }
                                            if (function_exists('wc_help_tip')) {
                                                echo wc_help_tip( $tooltip_html );
                                            } else {
                                                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                                            }
                                        }
                                    ?>
                                </div> 				
                            </td>
                            
                            <td>
                                <div>
                                    <?php
                                        if ($carrier->srv_zone_fr) {
                                            echo '<span class="zone zone_fr">'.__('FR', 'envoimoinscher') .'</span>';
                                        }
                                        if ($carrier->srv_zone_es) {
                                            echo '<span class="zone zone_es">'.__('ES', 'envoimoinscher') .'</span>';
                                        }
                                        if ($carrier->srv_zone_eu) {
                                            echo '<span class="zone zone_eu">'.__('EU', 'envoimoinscher') .'</span>';
                                        }
                                        if ($carrier->srv_zone_int) {
                                            echo '<span class="zone zone_int">'.__('INTER', 'envoimoinscher') .'</span>';
                                        }
                                        if (envoimoinscher_model::get_translation($carrier->srv_zone_restriction, $lang, true) != '') {
                                           $tooltip_html = envoimoinscher_model::get_translation($carrier->srv_zone_restriction, $lang, true);
                                           if (function_exists('wc_help_tip')) {
                                                echo wc_help_tip( $tooltip_html );
                                            } else {
                                                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                                            }
                                        }
                                    ?>
                                </div> 				
                            </td>
                            
                            <td class="destination">
                                <span class="type-img type-img-<?php echo $carrier->srv_dropoff_point; ?>"></span>
                                <span class="type type-<?php echo $carrier->srv_dropoff_point; ?>"><?php echo envoimoinscher_model::get_translation($carrier->srv_dropoff_place, $lang, true); ?></span>				
                            </td>
                            
                            <td class="destination">
                                <span class="type-img type-img-<?php echo $carrier->srv_pickup_point; ?>"></span>
                                <span class="type type-<?php echo $carrier->srv_pickup_point; ?>"><?php echo envoimoinscher_model::get_translation($carrier->srv_pickup_place, $lang, true); ?></span>		
                            </td>

                            <td class="delay"><?php echo envoimoinscher_model::get_translation($carrier->srv_delivery_due_time, $lang, true); ?></td>

                            <td class="status center">
                                <?php if ( $authorized_operators === false || in_array($carrier->ope_code, $authorized_operators ) ) : ?>
                                    <span>
                                        <input type="checkbox" name="offers[]" value="<?php echo $carrier_code; ?>" id="" <?php if ( 1 == $carrier->status ) { echo 'checked="checked"'; } ?> />
                                    </span>
                                <?php else : ?>
                                    <span class="disabled-carrier">
                                        <?php
                                            $tooltip_html = __('This carrier is not available for your content type:', 'envoimoinscher').'<br/>'.__($current_content['label'], 'envoimoinscher');
                                            if (function_exists('wc_help_tip')) {
                                                echo wc_help_tip( $tooltip_html );
                                            } else {
                                                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                                            }
                                        ?>
                                    </span>
                                <?php endIf; ?>             
                            </td>
                            
                            <td class="center" >
                                <?php if ( 1 == $carrier->status ) { ?>
                                    <a target="_blank" href="<?php echo admin_url('admin.php?page=wc-settings&tab=shipping&section='.strtolower($carrier_code))?>">
                                        <?php _e( 'Edit', 'envoimoinscher' ); ?>
                                    </a>
                                <?php } else {?>
                                -
                                <?php } ?>
                            </td>						
                        </tr>
                    <?php	
                        }	
                    }    
                ?>
			</tbody>
		</table>
        
        <!-- carrier reload popup -->
        <div style="display:none">
            <div id="carrier-reload-popup">
                <div class="center">
                    <span class="s14">
                        <?php
                            echo __( 'Would you like to update your front office carrier labels and descriptions as well?', 'envoimoinscher' ).'<br/>'; 
                            echo __( 'All your previous changes to those carrier settings will be lost.', 'envoimoinscher' ); 
                        ?>
                    </span>    
                    <br/><br/>
                    
                    <input type="submit" class="button-primary mr5" name="refresh_w_override" value="<?php _e( 'Yes, replace my current data', 'envoimoinscher' ); ?>" />
                    <input type="submit" class="button-primary mr5" name="refresh" value="<?php _e( 'No, keep as is', 'envoimoinscher' ); ?>" />
                </div>
            </div>
        </div>