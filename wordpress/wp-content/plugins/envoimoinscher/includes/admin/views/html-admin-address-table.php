<?php 
    $addressbook = envoimoinscher_model::get_addresses("all", true);
    $shipping_classes = EMC()->get_shipping_classes();
    $pick_dispo = array(
        'START' => array(
            '12:00' => '12:00', '12:15' => '12:15', '12:30' => '12:30', '12:45' => '12:45',
            '13:00' => '13:00', '13:15' => '13:15', '13:30' => '13:30', '13:45' => '13:45',
            '14:00' => '14:00', '14:15' => '14:15', '14:30' => '14:30', '14:45' => '14:45',
            '15:00' => '15:00', '15:15' => '15:15', '15:30' => '15:30', '15:45' => '15:45',
            '16:00' => '16:00', '16:15' => '16:15', '16:30' => '16:30', '16:45' => '16:45',
            '17:00' => '17:00'),
        'END' => array(
            '17:00' => '17:00', '17:15' => '17:15', '17:30' => '17:30', '17:45' => '17:45',
            '18:00' => '18:00', '18:15' => '18:15', '18:30' => '18:30', '18:45' => '18:45',
            '19:00' => '19:00', '19:15' => '19:15', '19:30' => '19:30', '19:45' => '19:45',
            '20:00' => '20:00', '20:15' => '20:15', '20:30' => '20:30', '20:45' => '20:45',
            '21:00' => '21:00')
    );
    
    $collect_hours = array( "1" => '1:00', "2" => '2:00', "3" => '3:00', "4" => '4:00',
        "5" => '5:00', "6" => '6:00', "7" => '7:00', "8" => '8:00', "9" => '9:00',
        "10" => '10:00', "11" => '11:00', "12" => '12:00', "13" => '13:00',	"14" => '14:00',
        "15" => '15:00', "16" => '16:00', "17" => '17:00', "18" => '18:00',	"19" => '19:00',
        "20" => '20:00', "21" => '21:00', "22" => '22:00', "23" => '23:00'
    );
?>
<table class="emc_addresses form-table">
    <tbody>
        <th scope="row" class="titledesc">
			<label for="default-address"><?php
                echo '<span class="mr2">'.__("Default address *", 'envoimoinscher').'</span>'; 
                $tooltip_html = __( "This address will be used if no address by shipping class is configured or found. It will also be the address used for products with no shipping class.", 'envoimoinscher' );
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></label>
		</th>
        <td id="default-address">
            <?php
                if (!empty($addressbook)) {
                    foreach ($addressbook as $address) {
                        if ($address['type'] != "default") continue;
 
                        EMC()->format_address($address);
                    }
                } else {
                    ?>
                        <a id="new-default-address" class="button-primary" title="<?php _e( 'Add default address', 'envoimoinscher' ); ?>">
                            <?php _e('Add address', 'envoimoinscher'); ?>
                        </a>
                    <?php
                }
            ?>
            
        </td>
    </tbody>
</table>

<table class="emc_addresses form-table">
    <tbody>
        <th scope="row" class="titledesc">
            <label for="additional-addresses"><?php
                echo '<span class="mr2">'.__("Addresses by shipping class", 'envoimoinscher').'</span>'; 
                $tooltip_html = __( "These addresses will be used depending on the shipping class of the products in the cart. When possible, the plugin will try to find a common shipping address for the cart to minimize shipping costs.", 'envoimoinscher' );
                if (function_exists('wc_help_tip')) {
                    echo wc_help_tip( $tooltip_html );
                } else {
                    echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                }
            ?></label>
		</th>
        <td id="additional-addresses">
            <?php
                if (!empty($addressbook)) {
                    foreach ($addressbook as $address) {
                        // skip default address
                        if ($address['type'] != "additional") continue;
                        
                        EMC()->format_address($address);
                    }
                    if (count($shipping_classes) >= 1) {
                        echo '<a id="new-additional-address" class="button-primary">'. __('Add address', 'envoimoinscher').'</a>';
                    } else {
                        _e('You must first setup shipping classes', 'envoimoinscher');
                    }
                } else {
                    _e('You must first setup a default address', 'envoimoinscher');
                }
            ?>
        </td>
    </tbody>
</table>

<!-- add address popup -->
<div style="display:none">
    <div id="address-popup">
        <span id="address-popup-content">
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php
                            echo '<span class="mr2">'.__( 'Address label *', 'envoimoinscher' ).'</span>';
                            $tooltip_html = __( "a unique label for this address", 'envoimoinscher' );
                            if (function_exists('wc_help_tip')) {
                                echo wc_help_tip( $tooltip_html );
                            } else {
                                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                            }
                        ?></th>
                        <td>
                            <input type="text" name="label" value="" tabindex="1" />
                        </td>
                        <th scope="row" colspan="2" class="titledesc">
                            <span class="required-fields-warning" style="display:none"><?php _e('Please fill in the required fields', 'envoimoinscher'); ?></span>
                        </th>
                    </tr>
                    <tr valign="top" class="shipping-classes-block">
                        <th scope="row" class="titledesc"><?php _e( 'Shipping classes *', 'envoimoinscher' ); ?></th>
                        <td>
                            <select name="shipping_classes" class="shipping-classes" multiple="multiple" tabindex="14">
                                <?php
                                    foreach ($shipping_classes as $class) {
                                        echo '<option value="'.$class->slug.'" selected="selected">'.$class->name.' ('.$class->slug.')</option>';
                                    }
                                ?>
                            </select>
                        </td>
                        <th scope="row" colspan="2" class="titledesc"></th>
                    </tr>
                </tbody>
            </table>
            <table class="form-table">
                <tbody>
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'Shipper title *', 'envoimoinscher' ); ?></th>
                        <td>
                            <select name="gender" class="gender" tabindex="2">
                                <?php
                                    $default = "M";
                                ?>
                                <option value="M" <?php if ($default == "M") echo 'selected="selected"'; ?>><?php _e( 'Mr.', 'envoimoinscher' ); ?></option>
                                <option value="Mme" <?php if ($default == "Mme") echo 'selected="selected"'; ?>><?php _e( 'Mrs', 'envoimoinscher' ); ?></option>
                            </select>
                        </td>
                        <th scope="row" class="titledesc"><?php _e( 'Pickup opening hour *', 'envoimoinscher' ); ?></th>
                        <td>
                            <select name="dispo_hde" class="pickup-dispo" tabindex="12">
                                <?php 
                                    $default = "12:00";
                                    foreach ($pick_dispo['START'] as $key => $value) {
                                        echo '<option value="'.$key.'" ';
                                        if ($key == $default) {
                                            echo 'selected="selected"';
                                        }
                                        $hrsmin = explode(':', $value);
                                        echo '>'.sprintf(__('%1$s:%2$s', 'envoimoinscher'), $hrsmin[0], $hrsmin[1]).'</option>';
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'Shipper first name *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="first_name" value="" tabindex="3" />
                        </td>
                        <th scope="row" class="titledesc"><?php _e( 'Pickup closing hour *', 'envoimoinscher' ); ?></th>
                        <td>
                            <select name="dispo_hle" class="pickup-dispo" tabindex="13">
                                <?php
                                    $default = "17:00";
                                    foreach ($pick_dispo['END'] as $key => $value) {
                                        echo '<option value="'.$key.'" ';
                                        if ($key == $default) {
                                            echo 'selected="selected"';
                                        }
                                        $hrsmin = explode(':', $value);
                                        echo '>'.sprintf(__('%1$s:%2$s', 'envoimoinscher'), $hrsmin[0], $hrsmin[1]).'</option>';
                                    }
                                ?>
                            </select>
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'Shipper last name *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="last_name" value="" tabindex="4" />
                        </td>
                        <th scope="row" class="titledesc">
                            <?php 
                                _e( 'Departure date *', 'envoimoinscher' );
                                $tip = __( 'Define how many days between order and shipment (drop-off or pick-up date).', 'envoimoinscher' );
                                echo '<img class="help_tip" data-tip="' . esc_attr( $tip ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                            ?>
                        </th>
                        <td>
                            <span><?php _e( 'D +', 'envoimoinscher' ); ?></span>
                            <select name="pickup_day_1" class="pickup-day">
                                <?php
                                    $i = 0;
                                    $default = "2";
                                    while ($i < 20) {
                                        echo '<option value='.$i.' ';
                                        if ( $i == $default ) echo 'selected="selected"';
                                        echo ">$i</option>";
                                        $i++;
                                    }
                                ?>
                                </select>
                            <span>
                                <?php _e( 'for orders submitted before', 'envoimoinscher' ); ?>
                                <select name="pickup_split" class="fromto-1">
                                    <?php
                                        $default = "17";
                                        foreach($collect_hours as $k => $v){
                                            echo '<option value='.$k.' ';
                                            if ( $k == $default ) echo 'selected="selected"';
                                            $hrsmin = explode(':', $v);
                                            echo '>'.sprintf(__('%1$s:%2$s', 'envoimoinscher'), $hrsmin[0], $hrsmin[1]).'</option>';
                                        }
                                    ?>
                                </select>
                            </span><br/><br/>
                            <span><?php _e( 'D +', 'envoimoinscher' ); ?></span>
                            <select name="pickup_day_2" class="pickup-day">
                                <?php
                                    $i = 0;
                                    $default = "3";
                                    while ($i < 20) {
                                        echo '<option value='.$i.' ';
                                        if ( $i == $default  ) echo 'selected="selected"';
                                        echo ">$i</option>";
                                        $i++;
                                    }
                                ?>
                                </select>
                            <span>
                                <?php _e( 'for orders submitted after', 'envoimoinscher' ); ?>
                                <select class="fromto-2">
                                    <?php
                                        $default = "17";
                                        foreach($collect_hours as $k => $v){
                                            echo '<option value='.$k.' ';
                                            if ( $k == $default ) echo 'selected="selected"';
                                            $hrsmin = explode(':', $v);
                                            echo '>'.sprintf(__('%1$s:%2$s', 'envoimoinscher'), $hrsmin[0], $hrsmin[1]).'</option>';
                                        }
                                    ?>
                                </select>
                            </span>
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'Company *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="company" value="" tabindex="5" />
                        </td>
                        <th scope="row" class="titledesc"><?php _e( 'Phone number *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="phone" value="" tabindex="9" />
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'Address *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="address" value="" tabindex="6" />
                        </td>
                        <th scope="row" class="titledesc"><?php _e( 'Email *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="email" value="" tabindex="10" />
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'ZIP code *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="zipcode" value="" tabindex="7" />
                        </td>
                        <th scope="row" rowspan=3 class="titledesc"><?php _e( 'Additional address information', 'envoimoinscher' ); ?></th>
                        <td rowspan=3>
                            <textarea name="add_info" tabindex="11"></textarea>
                        </td>
                    </tr>

                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'City *', 'envoimoinscher' ); ?></th>
                        <td>
                            <input type="text" name="city" value="" tabindex="8" />
                        </td>
                    </tr>
                    
                    <tr valign="top">
                        <th scope="row" class="titledesc"><?php _e( 'Shipping country', 'envoimoinscher' ); ?></th>
                        <td>
                            <?php 
                                $countries = EMC()->get_all_countries();
                                $shipping_country = get_option('EMC_COUNTRY');
                                if (isset($countries[$shipping_country])) {
                                    echo $countries[$shipping_country];
                                } else {
                                    echo $shipping_country;
                                }
                            ?>
                        </td>
                    </tr>
                </tbody>
            </table>
            <div class="actions">
                <button class="button-primary mr5" name="add-address">
                    <span class="add"><?php _e( 'Add this address', 'envoimoinscher' ); ?></span>
                    <span class="edit" style="display:none"><?php _e( 'Edit this address', 'envoimoinscher' ); ?></span>
                </button>
                <button class="button mr5" name="cancel"><?php _e( 'Cancel', 'envoimoinscher' ); ?></button>
            </div>
        </span>
    </div>
</div>

<!-- remove address popup -->
<div style="display:none">
    <div id="remove-address-popup">
        <div class="center">
            <span class="s14">
                <?php
                    echo __( 'Are you sure that you want to delete this address?', 'envoimoinscher' ).'<br/>';
                    echo __( 'You will have to choose a new address for orders already using this one.', 'envoimoinscher' );
                ?>
            </span>    
            <br/><br/>
            <div class="actions">
                <button class="button-primary mr5" name="remove-address">
                    <?php _e( 'Ok', 'envoimoinscher' ); ?>
                </button>
                <button class="button mr5" name="cancel"><?php _e( 'Cancel', 'envoimoinscher' ); ?></button>
            </div>
        </div>
    </div>
</div>