<?php

/**
 * Admin View: Offers simulator
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

?>

<div class="wrap woocommerce">
    <?php if(!empty($offers)): ?>
        <table class="emc_simulator_list wc_input_table widefat">
            <thead>
                <tr>
                    <th class="w11"><?php printf( __( 'Offer', 'envoimoinscher' ) ); ?></th>
                    <th class="w11"><?php printf( __( 'Carrier', 'envoimoinscher' ) ); ?></th>
                    <th class="w11"><?php printf( __( 'EMC price (tax excl.)', 'envoimoinscher' ) ); ?></th>
                    <th class="w11"><?php printf( __( 'Front office display (tax excl.)', 'envoimoinscher' ) ); ?></th>
                    <th><?php printf( __( 'Description', 'envoimoinscher' ) ); ?></th>
                </tr>
            </thead>
            <tbody>
            <?php foreach($offers as $o => $offre) { ?>
                <tr>
                    <td class="center label"><b><?php echo $offre['service_label'];?></b></td>
                    <td class="center"><?php echo $offre['operator_label'];?></td>
                    <td class="center"><?php echo $offre['emc_price'];?> <?php 
                        echo $offre['currency'] == 'EUR' ? '€' : $offre['currency']; 
                        echo '<br/>('.sprintf( _n( 'sent in %d shipment', 'sent in %d shipments', $offre['shipment_nb'], 'envoimoinscher' ), $offre['shipment_nb'] ).')';
                    ?></td>
                    <td class="center"><?php echo isset($offre['FO_display']) ? $offre['FO_display'] : __('No pricing rule was found', 'envoimoinscher');?></td>
                    <td class="hide_links"><?php echo implode('<br /> - ', $offre['characteristics']); ?></td>
                </tr>
            <?php } ?>
            </tbody>
        </table>
    <?php else: ?>
        <b><?php _e('No carrier found for this address', 'envoimoinscher'); ?></b>
    <?php endIf; ?>
</div>
