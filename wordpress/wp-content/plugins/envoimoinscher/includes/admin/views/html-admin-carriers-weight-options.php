<?php
/**
 * Admin View: Weight options
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

		?>
		<h3><?php _e( 'Multi-parcel and weight options', 'envoimoinscher' ); ?></h3>
		<p><b><?php _e( 'Customize the size of your parcels.', 'envoimoinscher' ); ?></b><br/>
		<?php _e( 'Our plugin uses the below table to match parcel dimensions with cart weight : product dimensions input in product edition pages are NOT taken into account.', 'envoimoinscher' ); ?><br/>
		<?php _e( 'Without customization, the default dimensions provided by Boxtal will be used.', 'envoimoinscher' ); ?></p>
		
        <!-- multiparcel option -->
        <table class="form-table">
            <tbody>
                <tr valign="top">
                    <th scope="row" class="titledesc">
                        <label for="EMC_PARCEL_SPLIT"><?php _e( 'Multi-parcel option', 'envoimoinscher' ); ?></label>
                        <?php
                            $tooltip_html = __( 'If you activate this option, customer cart items will be splitted into several parcels if needed. The <b>"Parcel splitting limit"</b> below lets you choose the maximum size beyond which a split will be attempted.', 'envoimoinscher' );
                            $tooltip_html .= ' '. __( 'All carriers <b>below the splitting limit</b> will be active in your shop if the cart can be split.', 'envoimoinscher' );
                            $tooltip_html .= ' '. __( '<b>Warning: shipments containing several parcels will have a higher Boxtal cost.</b>', 'envoimoinscher' );
                            if (function_exists('wc_help_tip')) {
                                echo wc_help_tip( $tooltip_html );
                            } else {
                                echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                            }
                        ?>
                    </th>
                    <td class="forminp forminp-select">
                        <select name="EMC_PARCEL_SPLIT">
                            <option value="0" <?php if (get_option('EMC_PARCEL_SPLIT', 1) == 0) echo 'selected="selected"'; ?>><?php _e( 'off', 'envoimoinscher' ); ?></option>
                            <option value="1" <?php if (get_option('EMC_PARCEL_SPLIT', 1) == 1) echo 'selected="selected"'; ?>><?php _e( 'on', 'envoimoinscher' ); ?></option>
                        </select>
                    </td>
	            </tr>
            </tbody>
        </table>
        
        <!-- weight option table -->
		<table class="<?php echo $current_subtab; ?> wc_input_table widefat">
			<thead>
				<tr>
					<th>#</th>
					<th style="min-width:90px"><?php _e( 'Weight', 'envoimoinscher' ); ?></th>
					<th style="min-width:100px"><?php _e( 'Max length', 'envoimoinscher' ); ?></th>
					<th style="min-width:100px"><?php _e( 'Max width', 'envoimoinscher' ); ?></th>
					<th style="min-width:100px"><?php _e( 'Max height', 'envoimoinscher' ); ?></th>
					<th><?php
                        echo '<span class="mr2">'.__('Active carriers upper limit', 'envoimoinscher').'</span>';
                        $tooltip_html = __( "Using the dimensions to the left, this column shows the upper limit beyond which each active carrier will no longer be available in your shop. Activating the multi-parcel option will keep parcels below the limit set to the right (if carts can be split) and thus make carriers available beyond their limit.", 'envoimoinscher' );
                        if (function_exists('wc_help_tip')) {
                            echo wc_help_tip( $tooltip_html );
                        } else {
                            echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                        }
                    ?></th>
                    <th style="min-width:170px"><?php
                        echo '<span class="mr2">'.__('Parcel splitting limit', 'envoimoinscher').'</span>';
                        $tooltip_html = __( "This is the upper limit set beyond which the module will try and split carts into several parcels. All carriers below this limit will be available if carts can be split.", 'envoimoinscher' );
                        if (function_exists('wc_help_tip')) {
                            echo wc_help_tip( $tooltip_html );
                        } else {
                            echo '<img class="help_tip" data-tip="' . esc_attr( $tooltip_html ) . '" src="' . WC()->plugin_url() . '/assets/images/help.png" height="16" width="16" />';
                        }
                    ?></th>
				</tr>
			</thead>
			<tbody>
				<?php foreach ( $dims as $dim ) {	?>
					<tr rel="<?php echo $dim->dim_id; ?>">
						<td class=""><?php echo $dim->dim_id;?></td>

						<td class="dimension">
							<input type="text" class="emc_input_decimal" name="weight<?php echo $dim->dim_id; ?>" id="weight<?php echo $dim->dim_id; ?>" value="<?php echo $dim->dim_weight; ?>" size="3" /> <span>kg</span>
						</td>

						<td class="dimension">
							<input type="text" class="emc_input_integer" name="length<?php echo $dim->dim_id; ?>" id="length<?php echo $dim->dim_id; ?>" value="<?php echo $dim->dim_length; ?>" size="3" /> <span>cm</span>
						</td>

						<td class="dimension">
							<input type="text" class="emc_input_integer" name="width<?php echo $dim->dim_id; ?>" id="width<?php echo $dim->dim_id; ?>" value="<?php echo $dim->dim_width; ?>" size="3" /> <span>cm</span>
						</td>
						
						<td class="dimension">
							<input type="text" class="emc_input_integer" name="height<?php echo $dim->dim_id; ?>" id="height<?php echo $dim->dim_id; ?>" value="<?php echo $dim->dim_height; ?>" size="3" /> <span>cm</span>
							<input type="hidden" name="id<?php echo $dim->dim_id; ?>" id="id<?php echo $dim->dim_id; ?>" value="<?php echo $dim->dim_id; ?>" />
						</td>
                        
                        <td class="carriers">
                            <?php
                                foreach($carrier_upper_limit[$dim->dim_id] as $carrier_code) {
                                    $carrier = envoimoinscher_model::get_service_by_carrier_code($carrier_code);
                                    list($operator, $service) = explode('_', $carrier_code);
                                    $img_file = plugin_dir_path(EMC_PLUGIN_FILE) . 'assets/img/carriers/logo-'.strtolower($operator).'.png';
                                    echo '<div class="carrier_logo">';
                                    if (file_exists($img_file)) {
                                        $img = plugins_url( '/assets/img/carriers/logo-'.strtolower($operator).'.png', EMC_PLUGIN_FILE );
                                        echo '<span class="carrier_logo '.$operator.'"><img src="'.$img.'"/></span>';
                                    }
                                    echo envoimoinscher_model::get_translation($carrier->srv_name_bo, $language, true);
                                    echo '</div>';
                                }
                            ?>
                        </td>
                        
                        <td>
                            <input type="radio" name="EMC_PARCEL_MAX_DIM" value="<?php echo $dim->dim_id; ?>" <?php if(get_option('EMC_PARCEL_MAX_DIM', 11) == $dim->dim_id) echo "checked"; ?> />
                        </td>
					</tr>
				<?php } ?>
			</tbody>
		</table>
        <div style="margin-top:5px;margin-bottom:10px;">
            <button name="add_weight_option_row" class="button"><?php _e('Add row', 'envoimoinscher'); ?></button>
            <button name="delete_weight_option_row" class="button" <?php if(count($dims) < 2) echo 'disabled="disabled"'; ?>><?php _e('Delete row', 'envoimoinscher'); ?></button>
        </div>