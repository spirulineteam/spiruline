<?php
/**
 * EMC Help page
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'emc_settings_help' ) ) :

/**
 * emc_settings_help
 */
class emc_settings_help extends emc_admin_settings {

	/**
	 * Constructor.
	 */
	public function __construct() {
		$this->id    = 'help';
		$this->label = __( 'Help', 'envoimoinscher' );
        add_filter( 'emc_settings_tabs_array', array( &$this, 'add_tab' ), 20 );
	}

    /**
	 * Get settings array
	 * @param $memory_limit limit ressources usage (for activation only)
	 * @return array
	 */
	public function get_settings($memory_limit = false) {


        $settings = array(
            0 => array(
                'section_id' => 'logs',
                'section_title' => __( 'Logs', 'envoimoinscher' ),
                'fields' => array(
                    0 => array(
                        'title' => __( 'Enable logs', 'envoimoinscher' ),
                        'desc' => __( 'Enables quotation logs for debugging. Logs can be found at WooCommerce > System status > Logs', 'envoimoinscher' ),
                        'id' => 'EMC_ENABLED_LOGS',
                        'type' => 'checkbox',
                        'default' => 'no',
                        'desc_tip' => true,
                    )
                )
            )
        );

		return $settings;
	}

    /**
	 * Override output
	 */
	public function output() {

		include_once 'faq.php';
        include_once dirname ( dirname (__FILE__) ) . '/views/html-admin-help.php';

        parent::output();

	}

    /**
	 * helper function to display tracking url list in help page
	 */
	public static function display_tracking_url_list() {
		$urls = envoimoinscher_model::$tracking_urls;
        $services = envoimoinscher_model::get_services();
        $lang = get_locale();

        $html = '<ul>';
        foreach ($services as $service) {
            if (isset($urls[$service->ope_code.'_'.$service->srv_code])) {
                $link = $urls[$service->ope_code.'_'.$service->srv_code];
                $html .= '<li>'.envoimoinscher_model::get_translation($service->srv_name, $lang, true);
                $html .= ' ('.envoimoinscher_model::get_translation($service->srv_name_bo, $lang, true).') - ';
                $html .= '<a href="'.$link.'" target="_blank">'.$link.'</a>';
                $html .= '</li>';
            }
        }
        $html .= '</ul>';

        echo $html;
	}

}

endif;
