<?php
/**
 * EMC Shipping Description Settings
 */

if ( ! defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

if ( ! class_exists( 'emc_settings_shipping_description' ) ) :

/**
 * emc_settings_shipping_description
 */
class emc_settings_shipping_description extends emc_admin_settings {

   /**
    * Constructor.
    */
    public function __construct() {
        $this->id = 'shipping-description';
		$this->label = __( 'Shipping description', 'envoimoinscher' );
        add_filter( 'emc_settings_tabs_array', array( &$this, 'add_tab' ), 20 );
	}

	/**
	 * Get settings array
	 * @param $memory_limit limit ressources usage (for activation only)
	 * @return array
	 */
	public function get_settings($memory_limit = false) {

		$category_list = envoimoinscher_model::get_categories();

		$settings = array(
            0 => array(
                'section_id' => 'shipment_types',
                'section_title' => __( 'Your shipments', 'envoimoinscher' ),
                'fields' => array(
                    0 => array(
                        'title' => __( 'Shipment content *', 'envoimoinscher' ),
                        'desc' => __( 'You must specify the contents of your shipments. This information is transmitted to the carriers choose the most accurate among labels available language from the dropdown list. If you check the apply for the package description box, you use the language selected as the description of the contents of all your mail (this will be the information to be forwarded to (x) the carrier (s)). This data will be displayed on the shipping page on which you can trigger the order to send a parcel. It may be amended before validation of the page. Do not check the box if you want to resume direct name of products shipped (name that is saved in your product catalog)', 'envoimoinscher' ),
                        'id' => 'EMC_NATURE',
                        'type' => 'content_type_select',
                        'default' => '',
                        'options' => $category_list,
                        'desc_tip' => true,
                        'required' => true,
                    ),
                    1 => array(
                        'title' => "",
                        'desc' => __( '<strong>Apply to parcel description.</strong><br/>Choose the most accurate label as this information will be given to carriers.<br/>If checked, this label will replace your product\'s name in all your shipments\' content description.', 'envoimoinscher' ),
                        'id' => 'EMC_CONTENT_AS_DESC',
                        'type' => 'checkbox',
                        'default' => 'no',
                        'desc_tip' => true,
                    ),
                    2 => array(
                        'title' => __( 'Packaging *', 'envoimoinscher' ),
                        'desc' => __( 'Packaging must be specified for Colissimo offers. Additional charges may apply.', 'envoimoinscher' ),
                        'id' => 'EMC_WRAPPING',
                        'type' => 'hidden_select',
                        'default' => '1-Boîte',
                        'options' => array( '1-Boîte' => __( 'Box', 'envoimoinscher' ), '17-Tube' => __( 'Tube', 'envoimoinscher' )),
                        'required' => true,
                    ),
                ),
            ),
            1 => array(
                'section_id' => 'weight_setup',
                'section_title' => __( 'Product weight setup', 'envoimoinscher' ),
                'fields' => array(
                    0 => array(
                        'title' => __( 'Default weight (kg)', 'envoimoinscher' ),
                        'desc' => __( 'You can specify a default weight that will be applied on missing product weights (product weight is set in Products -> Edit product -> Shipping).', 'envoimoinscher' ),
                        'id' => 'EMC_AVERAGE_WEIGHT',
                        'type' => 'float',
                        'default' => '',
                        'desc_tip' => false,
                    ),
                ),
            ),
            2 => array(
                'section_id' => 'insurance_options',
                'section_title' => __( 'Insurance', 'envoimoinscher' ),
                'fields' => array(
                    0 => array(
                        'title' => __( 'Use AXA insurance', 'envoimoinscher' ),
                        'desc' => __( '<strong>Be careful that the cost of insurance is not automatically billed to your customers, and that if you choose this option, it will be automatically selected for all your orders.</strong><br/>By selecting declared value insurance, you declare to you have read <a href="http://www.boxtale.co.uk//faq/131-tout-savoir-sur-l-assurance-ad-valorem.html/notice/" target="_blank">AXA insurance declared value policy notice</a>. As packaging insufficiency and maladjustment are excluded risks of AXA warranty, you could benefit from some extra packaging.', 'envoimoinscher' ),
                        'id' => 'EMC_ASSU',
                        'type' => 'checkbox',
                        'default' => 'no',
                        'desc_tip' => true,
                    ),
                ),
            ),
		);

		return $settings;
	}

    /**
	 * Output content type select field.
	 *
	 * @param $field
	 */
	public function output_content_type_select_field ($field){
		$option_value = WC_Admin_Settings::get_option( $field['id'], $field['default'] );

        ?><tr valign="top" class="<?php echo sanitize_title( $field['type'] ) ?>">
            <th scope="row" class="titledesc">
                <label for="<?php echo esc_attr( $field['id'] ); ?>"><?php echo esc_html( $field['title'] ); ?></label>
                <?php echo $field['tooltip_html']; ?>
            </th>
            <td class="forminp forminp-<?php echo sanitize_title( $field['type'] ) ?>">
                <select
                    name="<?php echo esc_attr( $field['id'] ); ?>"
                    id="<?php echo esc_attr( $field['id'] ); ?>"
                    style="<?php echo esc_attr( $field['css'] ); ?>"
                    class="<?php echo esc_attr( $field['class'] ); ?>"
                    <?php echo implode( ' ', $field['attributes'] ); ?>
                    >
                    <?php
                        echo '<option>'.__('-- Please choose one --', 'envoimoinscher').'</option>';
                        foreach ( $field['options'] as $category_group_id => $category_group ) {
                            echo '<optgroup label="'.$category_group['name'].'">';
                            foreach($category_group['categories'] as $cat_id => $content) {
                                echo '<option value="'.esc_attr($cat_id).'" ';
                                if ($cat_id == $option_value) {
                                    echo 'selected="selected" ';
                                }
                                if ($content['forbidden'] == true) {
                                    echo 'disabled="disabled"';
                                }
                                echo '>'.$content['label'].'</option>';
                            }
                            echo '</optgroup>';
                        }
                    ?>
               </select> <?php echo $field['description']; ?>
            </td>
        </tr><?php
	}

    /**
	 * Save content type select field.
	 *
	 * @param $field
	 */
	public function save_content_type_select_field ($field) {
        update_option( $field['id'], isset($_POST['EMC_NATURE']) ? $_POST['EMC_NATURE'] : '');
        envoimoinscher_model::remove_carriers_forbidden_categories();
	}
}

endif;
