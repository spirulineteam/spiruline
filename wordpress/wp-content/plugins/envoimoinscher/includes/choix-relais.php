<?php
$cp = isset($_REQUEST['cp']) && !empty($_REQUEST['cp']) ? $_REQUEST['cp'] : get_option('EMC_POSTALCODE') ;
$ville = isset($_REQUEST['ville']) && !empty($_REQUEST['ville'])? $_REQUEST['ville'] : get_option('EMC_CITY') ;
$country = isset($_REQUEST['country']) ? $_REQUEST['country'] : 'FR' ;
$collecte = isset($_REQUEST['collecte']) ? $_REQUEST['collecte'] : 'exp' ;
$srv = isset($_REQUEST['srv']) ? $_REQUEST['srv'] : '' ;
$ope = isset($_REQUEST['ope']) ? $_REQUEST['ope'] : '' ;
$carrier_code = $ope.'_'.$srv;
$inputCallBack = isset($_REQUEST['inputCallBack']) ? $_REQUEST['inputCallBack'] : '' ;
$type = isset($_REQUEST['type']) ? $_REQUEST['type'] : 'html' ;

if ( $type == "json") {// I'm AJAX!
    header("content-type:application/json");
    $listPoints = new Emc\ListPoints();
    $language = get_locale();
    $listPoints->setLocale(str_replace('_', '-', $language));
    $upload_dir = wp_upload_dir();
    $listPoints->setUploadDir($upload_dir['basedir']);
    $listPoints->getListPoints(
        array($carrier_code),
        array('ville' => $ville, 'cp' => $cp, 'pays' => $country, 'collecte' => $collecte)
    );
    $points = $listPoints->list_points;
    die(json_encode($points));
} else {
    $aRelayName = array('CHRP'=>'Chrono Relais', 'MONR'=>'Point Relais', 'SOGP'=>'Relais Colis', 'UPSE'=>'Relais Kiala', 'PUNT'=>'Punto Pack');
    $relayName = isset($aRelayName[strtoupper($ope)])? $aRelayName[$ope] : 'Point Relais';

    // Script URL
    $baseUrl = admin_url()."/admin-ajax.php?action=get_choix_relais&";
    $pluginUrl = plugins_url('', EMC_PLUGIN_FILE);
    
    $lang = array(
      'Unable to load parcel points' => __( 'Unable to load parcel points', 'envoimoinscher' ),
      'day_1' => __( 'monday', 'envoimoinscher' ),
      'day_2' => __( 'tuesday', 'envoimoinscher' ),
      'day_3' => __( 'wednesday', 'envoimoinscher' ),
      'day_4' => __( 'thursday', 'envoimoinscher' ),
      'day_5' => __( 'friday', 'envoimoinscher' ),
      'day_6' => __( 'saturday', 'envoimoinscher' ),
      'day_7' => __( 'sunday', 'envoimoinscher' ),
      'Opening hours' => __( 'Opening hours', 'envoimoinscher' ),
      'Search' => __( 'Search', 'envoimoinscher' ),
      'Choose this ' => __( 'Choose this ', 'envoimoinscher' ),
      'Code of' => sprintf(__( '%s code:', 'envoimoinscher' ), $relayName)
    );

    wp_enqueue_style( 'admin_relay_choice', '//maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css', array(), '3.3.6');
    wp_enqueue_script( 'gmap', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBLvWeSENu0h4lDozEYIOaAbMbgVtS9EWI', array('jquery') );
    wp_enqueue_script( 'admin_relay_choice', plugins_url( '/assets/js/choix-relais.js', EMC_PLUGIN_FILE ), array( 'jquery', 'gmap' ), EMC_VERSION );
    wp_localize_script( 'admin_relay_choice', 'lang', $lang );
    wp_localize_script( 'admin_relay_choice', 'pluginUrl', $pluginUrl );
    wp_localize_script( 'admin_relay_choice', 'baseUrl', $baseUrl );
    wp_localize_script( 'admin_relay_choice', 'relayName', $relayName );
    wp_localize_script( 'admin_relay_choice', 'ptrelOpe', $ope );
    wp_localize_script( 'admin_relay_choice', 'ptrelSrv', $srv );
    wp_localize_script( 'admin_relay_choice', 'ptrelPoids', "0.72" );
    wp_localize_script( 'admin_relay_choice', 'ptrelPays', $country );
    wp_localize_script( 'admin_relay_choice', 'ptrelCollecte', $collecte );
    wp_localize_script( 'admin_relay_choice', 'ptrelInputCallBack', $inputCallBack );  
    ?>
    <!DOCTYPE HTML>
    <html <?php language_attributes(); ?>>
    <head>
        <meta charset="UTF-8">
        <title>Choix <?php echo $relayName; ?></title>
        <style type="text/css">
            body {
              background-color: #fff;
              font-family: "Helvetica Neue",Helvetica,Arial,sans-serif;
            }
            .pt20 {
                padding-top: 20px;
            }
            .pb20 {
                padding-bottom: 20px;
            }
            .pl40 {
                padding-left: 40px;
            }
            .pr40 {
                padding-right: 40px;
            }
            .mb20 {
                margin-bottom: 20px;
            }
            .mt20 {
                margin-top: 20px;
            }
            .pointer {
                cursor: pointer;
            }
            .bg-top-box {
                background: #FFF;
                padding-top: 10px;
            }
            .popover-hour + .popover {
                max-width: 300px;
            }
            .db{
                display:block;
            }
            #submitNewMap {
                text-transform: uppercase;
            }
            .day {
                display: inline-block;
                width: 60px;
                font-weight: bold;
            }
            .btn-primary {
                background-color: #0073aa;
            }
            .emcMarker {
                max-height:30px;
                margin-right:5px;
                vertical-align:middle;
            }
            .emcPointTitle {
                text-overflow: ellipsis;
                overflow: hidden;
                white-space: nowrap;
                width: 170px;
                display: inline-block;
                vertical-align: middle;
            }
            .emcPointTitle a {
                color: #337ab7;
            }
            .emcPointTitle a:hover {
                text-decoration:none;
            }
            #rightcol-ptrel {
                font-size: 14px;
            }
            table {
                border:0;
            }
            table td {
                border:0;
                border-top:1px solid #ddd;
            }
        </style>
    </head>
    <body>
        <div class="container" style="width:100%;">
            <div class="row">
                <div class="col-xs-12">
                        <div class="bg-top-box pt20 pb20 pl40 pr40">
                            <div id="map" class="mt20">
                                <div class="row mb20">
                                    <div class="col-xs-3">
                                        <input type="text" id="ptrel-cp" class="form-control input-sm" placeholder="Code Postal" value="<?php echo $cp; ?>"/>
                                    </div>
                                    <div class="col-xs-4">
                                        <input type="text" id="ptrel-ville" class="form-control input-sm" placeholder="Ville" value="<?php echo $ville; ?>"/>
                                    </div>
                                    <a class="pointer" >
                                        <div id="submitNewMap" class='col-xs-3 btn btn-primary btn-sm' style="width:160px;"><?php _e( 'Search', 'envoimoinscher' ) ?></div>
                                    </a>
                                </div>
                                <div class="row">
                                    <div class="col-xs-9">
                                        <div id="map-canvas" style="height:500px;"></div>
                                    </div>
                                    <div class="col-xs-3 pl10"  style="height:500px;overflow-y:scroll">
                                        <table class="table table-hover">
                                            <tbody id="rightcol-ptrel">
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                </div><!-- eod col-xs-9  -->
            </div><!-- eod row bg-grey  -->
        </div><!-- eod container  -->
    </body>
    <footer>
    <?php wp_footer(); ?> 
    </footer>
    </html>
    <?php
}
?>