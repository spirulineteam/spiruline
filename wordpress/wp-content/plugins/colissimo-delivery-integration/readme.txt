===Colissimo Delivery Integration === 
Contributors: Harasse
Tags: woocommerce, colissimo, shipping, laposte, parcel, logistic, tracking
Tested up to: 4.9.4
Requires at least:  4.4
Stable tag: trunk
License: GPLv3
License URI: https://www.gnu.org/licenses/gpl-3.0.html

Une intégration complète de Colissimo dans WooCommerce.

== Description ==

= Fonctions : =

Colissimo Delivery Integration réalise l'intégration des services Colissimo (groupe La Poste) dans WooCommerce. Colissimo Delivery Integration (CDI) permet:

* L’utilisation d’une méthode de livraison Woommerce puissante et bien adaptée à Colissimo (sélection des tarifs par classes de produit, fourchette de prix du panier HT ou TTC, fourchette de poids des articles ; tarifs variables programmables ; gestion des coupons promo Woocommerce ; mode inclusion/exclusion pour les classes produit et les coupons promo ; gestion de tarifs prioritaires).
* Au client son choix de méthode de livraison. Ses données de suivi colis figurent dans les courriels et dans ses vues Woocommerce de commandes. Il dispose d’une extension de l’adresse basique Woocommerce (2 lignes) aux standards postaux 4 lignes.
* La gestion de toutes les options Colissimo: signature, assurance complémentaire, expéditions internationales, type de retour, caractéristiques CN23, … . Le service des retours de colis par le client avec pilotage/autorisation par l’Administrateur. Le traitement des points relais avec carte Google Maps.
* La gestion des colis des commandes produites par Woocommerce dans une passerelle dédiée, asynchrone du flux Woocommerce, avec 3 modes de traitement possibles :
  - Mode manuel par l’export d’un csv permettant ensuite à l’Administrateur l’automatisation de scripts pour le logiciel du transporteur qu’il utilise ;
  - Mode automatique qui exécute en ligne le Web service Colissimo affranchissement de Colissimo pour récupérer automatiquement les étiquettes et autres données émises par Colissimo ;
  - Mode personnalisé qui active un filtre Wordpress pour passer les données des colis à une application propre au gestionnaire du site, lui permettant ainsi de s’adapter au protocole de son transporteur (qui peut être différent de Colissimo).
* Une gestion automatisée au maximum des colis dans la passerelle : soumission en 1 clic des colis au transporteur, purge automatique des colis traités, vue directe des étiquettes et cn23, impression globale des étiquettes, export global des colis, historique des colis.
* Un suivi temps réel dans la console d’administration des commandes, de la situation de délivrance des colis expédiés.
* Une utilisation des grandes fonctions à la carte du gestionnaire du site selon son besoin, et si nécessaire un paramétrage des codes produit Colissimo et des pays admis aux offres du transporteur.

= Utilisation : =

Les fonctions décrites ci-dessus se répartissent en 2 niveaux de service :

* Un service standard en utilisation libre, qui permet déjà d’atteindre un bon niveau d’automatisation,
* Un service CDI+ qui nécessite un enregistrement des utilisateurs, pour les fonctions additionnelles à plus forte valeur.

= Assistance et support : =

Le support du service standard est assuré par les réponses des participants au forum wordpress.org .
Les utilisateurs du service CDI+ disposent d’un support spécifique et personnalisé.

== Installation et démarrage ==

1. Installer le plugin de façon traditionnelle, et l’activer.
1. Aller dans la page  : Woocommerce -> Réglages -> Colissimo, et adapter quand nécessaire les réglages des 8 onglets : Réglages généraux, CN23, Interface client, Mode automatique, Références aux livraisons, Méthode Colissimo, Retour colis, Impression d'étiquettes. Bien veiller à enregistrer un à un chacun des onglets. Les réglages par défaut de CDI permettent déjà un fonctionnement immédiat.
1. Renseigner vos réglages de l'identifiant et du mot de passe Colissimo, ainsi que de la clé Google Maps, si vous utilisez ces fonctions.
1. Aller dans la page Woocommerce des commandes et cliquer sur l’icône «colis ouvert» d’une commande pour créer dans la passerelle CDI un colis à expédier. Si besoin avant cela, les caractéristique du colis peuvent être visualisées et modifiées en ouvrant la commande et en modifiant sa zone «Métabox CDI».
1. Aller dans la  passerelle CDI  Woocommerce -> Colissimo , pour retrouver les colis à expédier.
1. Cliquer sur le bouton «Automatique» pour soumettre au serveur Colissimo tous les colis en attente, ce qui retournera les étiquettes d’affranchissement.

== Frequently Asked Questions ==

= Où puis-je obtenir de l'aide ou  parler à d'autres utilisateurs ?  =

* Le support gratuit est disponible à l'aide de la communauté dans le [Forum de Colissimo Delivery Intégration](https://wordpress.org/support/plugin/colissimo-delivery-integration).
C'est le meilleur moyen parce que toute la communauté profite des solutions données dans le forum. Vous pouvez utiliser indifféremment l'anglais ou le français. 
Si vous étés enregistré à CDI+ , vous bénéficiez en plus d’un support premium pour vous assister. 

= Puis-je obtenir une personnalisation poussée de CDI ? =

Une personnalisation de base peut être obtenue par les paramètres CDI. Mais vous pouvez allez plus loin et avoir une personnalisation beaucoup plus fine lorsque vous utilisez des filtres Wordpress installés dans les fichiers  CDI. La règle d'utilisation de chacun de ces filtres est donnée par son utilisation dans les fichiers php activant ces filtres. Des exemples d'utilisation sont donnés dans le fichier includes/WC-filter-examples.php.

= En mode automatique (service web), quelle est la règle d'affectation du code de produit Colissimo ? =

* Le code produit affiché dans la Metabox Colissimo de la commande a toujours la priorité. Il peut être forcé manuellement dans la Metabox Colissimo. Si le code produit n'existe pas dans la Metabox Colissimo, le mode automatique utilisera les paramètres par défaut définis dans Woocommerce-> Réglages-> Colissimo (France, Outre-mer, Europe, International)
* Le code produit inséré dans la Metabox Colissimo  peut être initialisé lors de l'analyse d'une méthode d'expédition: soit il correspond à une méthode d'expédition définie dans Woocommerce-> Réglages-> expédition-> Colissimo; soit un code produit est retourné dynamiquement par Colissimo "Point de livraison - Pickup locations" service web.

= Où sont les panneaux de réglages/controle de CDI ? =

* Les panneaux sont en 3 endroits:
   -Dans Woocommerce-> Réglages-> Colissimo pour les réglages généraux, cn23, infos clients, mode automatique, références aux livraisons, méthode Colissimo, retour colis, impression d'étiquettes
   -Dans Woocommerce-> Réglages-> Expédition et dans chaque instance en zone d'expédition pour les paramètres relatifs à une méthode d'expédition Colissimo.
   -Dans Woocommerce-> Colissimo pour contrôler la passerelle pour la production d'étiquettes de colis et la gestion des colis.

= Comment effectuer des tests et se mettre en mode log/debug ? =

* Configurez votre fichier wp-config.php en mode débogage en insérant, à la place de la ligne "define ('WP_DEBUG', false);" , ces 3 lignes : define ('WP_DEBUG', true); Define ('WP_DEBUG_LOG', true); Define ('WP_DEBUG_DISPLAY', false); 
* Activez le paramètre "log for debugging purpose" dans les paramètres de CDI, et choississez les modules pour lesquels vous voulez un log. Après exécution de votre séquence à tester, affichez le fichier wp-content/debug.log pour voir les traces. Si vous afficher votre fichier debug.log dans le forum: a) supprimez le fichier debug.log avant votre test pour réduire sa longueur b) pour votre sécurité supprimez dans votre fichier toutes vos données contractNumber et password.
* N'oubliez pas lorsque votre site passe de test en exploitation de restaurer votre fichier wp-config.php avec "define ('WP_DEBUG', false);".

= Puis-je utiliser un transporteur différent de Colissimo ? =

* Non pour le mode automatique (Web Service). Ainsi que pour les fonctions spécifiques comme point de retrait.
* Oui, c'est une possibilité pour le mode manuel et le mode personnalisé, pour la méthode de livraison, et pour les infos rendues au client. Toutefois, les données passées dans la passerelle respecteront les normes Colissimo (signature, montant du remboursement, ...).

= A quoi sert le paramètre "Nettoyage automatique des colis dans la passerelle Colissimo" des paramètres généraux? =

* La Metabox Colissimo des commandes WooCommerce sont automatiquement mises à jour des code de suivi et autres informations depuis la passerelle. Le commutateur d'état est positionner à "Dans le camion" dans la Metabox. Cependant, les paquets Colissimo présents dans la passerelle sont supprimés automatiquement après ces opérations, uniquement si le paramètre "Nettoyage automatique des colis dans la passerelle Colissimo" est activé. C’est le mode de fonctionnement recommandé.
* Ne pas positionner ce paramètre peut être toutefois utile lorsque l'administrateur souhaite gérer manuellement les colis par des sessions à sa main; Le nettoyage de la passerelle doit alors être effectué manuellement.




== Screenshots == 

1. CDI global architecture.
1. CDI parameters in Woocommerce settings panel.
1. Orders page of Woocommerce.
1. Colissimo box in order details panel.
1. Colissimo gateway panel.
1. Colissimo shipping settings (shipping methods).
1. Colissimo shipping settings (Points de livraison - Pickup location).
1. Checkout page with Pickup locations list.
1. Checkout page, zoom on a locations.

== Changelog == 

= 3.1.0 (2018-03-11) =
* Fix Auto protect if FPDF class already exist
* CDI+ : Typo and fix 

= 3.0.0 (2018-03-06) =
* Tweak Optimizing  secondary key lenght in cdi table
* Add Ftd (Outre-mer) parameter
* Add Return-receipt parameter
* Add Order private status capacity to update CDI Metabox from gateway (new filter)
* Tweak Control pattern on sender zipcode
* Add Option setting to refresh GMap in checkout
* Tweak Checkout idle status change from 20s to 300s
* Add More settings available for string translation
* Add Examples for insurances
* Add Example for order private status
* Add Order customer message in gateway outputs
* Fix Css adaptation with WC 3.3.1
* Fix Shipping icon display in cart and checkout
* CDI+ : Start of the new option 
* CDI+ : Premium support for CDI+ registered users
* CDI+ : Colissimo shipping methods can be selected or excluded with WC Promo codes 
* CDI+ : Customizable extended Termid list for Colissimo shipping method
* CDI+ : Gateway view of attached pdf label and cn23 (and no more from url)
* CDI+ : Bulk printing of label and CN23 in process in the gateway
* CDI+ : Export csv of parcels in process in gateway
* CDI+ : Export csv history of parcels already send 
* CDI+ : Online postal tracking in CDI Metabox
* CDI+ : Extend of WC customer address to postal standarts (4 lines)
* Some typo and fix

= 2.0.6 (2017-08-13) =
* Add warnings in settings if update to do : WP, WC, PHP, Openssl
* Tweak Resize GM windows when multiple hidden div in checkout
* Fix Limit to 12 pickups in map display to avoid GM crash 
* Fix Sanitize pickup address before GM to avoid GM crash
* Tweak New algo to avoid multiple replay of GM
* Add Verify htttps access to pointretrait Colissimo website
* Fix Capability for CDI 
* Some typo and fix

= 2.0.3 (2017-07-09) =
* Fix Pickup station compatibility with WPML

= 2.0.2 (2017-07-02) =
* Fix status in CDI DB
* Fix Compatibility with Sequential Order Numbers
* Add Do_action 'cdi_actionorderlist_after_updateorder'

= 2.0.1 (2017-04-23) =
* Fix Add prefix to plugin_action_links (plugins conflicts)
* Tweak Change save_post hook prority to 99 (WooCommerce PDF Invoices & Packing Slips conflict)

= 2.0.0 (2017-04-09) =
* Fix WC 3.x compatibility with CRUD classes
* Add Proxy CDI server call structure for Web services
* Some typo and fix

= 1.18.4 (2017-03-19) =
* Fix CDI capability roles

= 1.18.3 (2017-03-12) =
* Tweak Pickup select width
* Add Reset official Colissimo settings
* Add Get product cn23 hstariff meta
* Some typo and fix

= 1.18.2 (2017-02-26) =
* Tweak Google map width optimizing
* Add Examples directory
* Some typo and fix

= 1.18.1 (2017-02-12) =
* Fix iMacros Coliship script (adapt to new Coliship html)

= 1.18.0 (2017-02-12) =
* Tweak factory default page layout for address labels printing
* Fix iMacros Coliship script
* Fix iMacros Online script
* Add Initialisation tariffs file in shipping méthod settings
* Add Filters for cn23 before creating Metabox
* Some typo and fix

= 1.17.0 (2017-01-29) =
* Add Print address labels gateway mode 
* Fix Data cleaned on plugin suppress 
* Tweak Google map width optimizing
* Tweak Redefine factory default settings 
* Some typo and fix

= 1.16.6 (2017-01-08) =
* Tweak Addressing errors details for Colissimo and Google Maps
* Fix Google Maps API key when geocoding
* Some typo and fix

= 1.16.5 (2016-12-18) =
* Add Instance selection capacity for exclusive method
* Add A no Class selection capacity in Colissimo shipping method
* Fix Customer warning when invalid sender address detected by Colissimo or googlemap
* Add Filter to choose SelectPickup and GoogleMap emplacement on screen
* Fix Display GoogleMap at switch of 2 pickup shipping tariffs

= 1.16.4 (2016-12-11) =
* Fix Decimalization of TotalAmount InsuranceValue and Cn23ArtValue
* Fix When CN23 Art Value is 0€
* Fix iMacros scripts for online mode and coliship mode
* Fix Gateway autoclean
* Tweak Max nb of CN23 art extended to 100 (but limit stay to 10 for online mode)
* Add Filters in Gateway for parcels sort and destination display
* Some typo and fix

= 1.16.2 (2016-12-04) =
* Tweak Rework of Colissimo gateway
* Tweak Freeze parcels capacity in Colissimo gateway
* Fix Point Retrait with CDI defaut weight = 0
* Add Php code sequence in tariff variable area
* Some typo and fix

= 1.16.1 (2016-11-27) =
* Fix Productcode and pickup relay for not logged customers
* Tweak Rework of Colissimo gateway
* Tweak GoogleMaps only Ajax triggered to avoid conficts
* Add Tracking code updatable in Colissimo Metabox
* Add Order datetime in array_for_carrier
* Add filter on company field for auto, online, and Coliship gateway
* Add "Cart price all taxes included" mode in shipping method
* Some typo and fix

= 1.16.0 (2016-11-20) =
* New Coliship mode in Gateway
* Fix Foreach array for PHP7
* Fix Forced product codes algo
* Tweak change parcel return settings 
* Add international parcel return
* Some typo and fix

= 1.15.3 (2016-11-13) =
* Tweak CDI status "intruck" as retour-colis eligibility (no more WC status "completed")
* Tweak new setting for Colissimo retour url
* Add new filter for retour-colis eligibility
* Fix label and cn23 display when stream transmitted (not base64)
* Fix suppress MailBoxPicking request in Retour-colis
* Some typo and fix

= 1.15.1 (2016-11-07) =
* Fix datas in personalized support function
* Fix init options module to log and role name for gateway
* Some typo and fix

= 1.15.0 (2016-11-06) =
* Tweak CDI log mecanism - Multi select of modules to log
* Tweak Customer mails - Hook change to avoid conflict with WCDN plugins (further analysis to do) 
* Tweak WS product codes new algorithm when not in metabox
* Tweak Retour-colis product code selected from settings
* Fix Product codes to associate to shipping - Some rework choix-livraison 
* Fix Country pickup list controled in choix-livraison
* Add filter to custom datas before building of Colissimo Metabox
* Add Setting to allow WP roles to use the CDI gateway
* Some typo and fix

= 1.14.3 (2016-10-22) =
* Add optional Google maps API key
* Fix invalid offset when not shipping zone method in the package list
* Fix sanitize phone
* Fix sanitize FR mobilephone (WS Colissimo)
* Some typo and fix

= 1.14.2 (2016-10-16) =
* Add multi select shipping classes in shipping method
* Fix slug of shipping class selected
* Add multi-site compatibility
* Some typo and fix

= 1.14.1 (2016-10-09) =
* Add an exclude mode option for shipping class sélection
* Fix mobile number in automatic mode
* Add styles googlemap setting in filter
* Fix respect of standarts for woocommerce address lines
* Some typo and fix

= 1.14.0 (2016-10-02) =
* Add uninstall data clean option
* Add a click in gateway url
* Fix no pickup when only virtual products
* Fix variable shipping fare when kg in woocommerce setting
* Some typo and fix

= 1.13.2 (2016-09-18) =
* Add docs in settings
* Add a not registered settings warning
* Fix error #30204 
* Some typo

= 1.13.1 (2016-09-06) =
* Add an exclusive capacity for shipping methods
* Fix product code exceptions

= 1.13.0 (2016-08-31) =
* Control PHP not less than 5.4.0
* Rework of pickup selection and cdi_pickuplocationlabel
* Add Filters for frontend customization (examples in WC-filter-examples.php)
* Add letter box deposit for online gateway
* Uppercase online gateway output
* Some typo and fix

= 1.12.1 (2016-08-20) =
* Add a personalized support function
* Add setting "Pickup location map open"
* Some internal rework
* Some typo and fix

= 1.12.0 (2016-08-13) =
* Add Colissimo "retour colis"
* Translation frontend
* Some typo and fix

= 1.11.1 (2016-07-31) =
* Tuning pickup location in mail and customer order view
* Add pickup location in admin colissimo meta box 
* Add Compute Colissimo MetaBox for on-hold status orders
* Complete French translation of admin colissimo meta box 
* Some typo and fix

= 1.11.0 (2016-07-27) =
* Add cn23 weight supports product variation
* Add up to 10 article lines in cn23
* Add Internationalization of the plugin 
* Add french localization
* Fix activate/enabling shipping method instance
* Some typo and fix

= 1.10.2 (2016-07-23) =
* Add instance selection capacity in method referrals settings
* Add woocommerce weight product unit conversion (kg/g)
* Fix product weight for product variation
* Add product code and pickup location in manual and custom gateways
* Some typo and fix

= 1.10.0 (2016-07-10) =
* Add map view of pickup locations
* Add exception product codes in settings (auto mode)

= 1.9.0 (2016-07-03) =
* Add optionally shipping icons in cart and checkout
* Add setting to choose Colissimo Pickup locations type
* Add pickup location label in customer view order
* Some tweaks and fix

= 1.8.0 (2016-06-26) =
* Compatibility with WC 2.6 
* Colissimo shipping method is now shipping zones mode compatible (need WC 2.6 or further) 
* New organization of settings
* Fix pickup location process for unauthenticated users
* Some tweaks and fix

= 1.7.1 (2016-06-18) =
* Add Prefix method name for frontend
* Fix for return choice (auto mode)
* Fix Add rates
* Some tweaks and fix

= 1.7.0 (2016-06-10) =
* Major update
* Add Colissimo "Choix de livraison - Pickup location" web service 
* Add Colissimo multi purpose shipping method (countries, weight, price)
* Some tweaks and fix

= 1.6.0 (2016-05-30) =
* Some internal rework and fix
* Extend Colissimo metabox in order display
* Extend settings for web service (pickup location contries and product code)
* Add pickup location and product code web service possibilities

= 1.5.0 (2016-05-22) =
* Add in settings an option for Colissimo to return or not the cn23 labels
* Add parcelNumberPartner when returned in Colissimo response
* Add and display label pdf and cn23 pdf when returned in Colissimo response
* Rework of Colissimo meta box

= 1.4.1 (2016-05-15) =
* Tweak - correct versioning bug (need no more to desactivate/activate plugin after update)

= 1.4.0 (2016-05-15) =
* Major update
* Add Url to labels in gateway
* Add Automatic mode - web service 
* Add optionnal insert of tracking code in customer emails and customer order view
* Add 'url to labels' return from custom mode filter
* Add 'cn23_shipping' in custom mode filter  and in order display 
* Change cn23 category internal codification for international practice
* Some tweaks 

= 1.3.2 (2016-04-25) =
* Tweak - Compatibility with WP 4.5 (WC-Metabox-Colissimo.php)

= 1.3.1 (2016-02-15) =
* Tweak - Colissimo status.

= 1.3.0 (2016-02-06) =
* Internal rework.
* Fix - Suppress spaces in tracking codes.
* Fix - Refresh tracking codes shown in order view.
* Fix - iMacros script error at add dest.
* Tweak - Change values for return type.
* Tweak - Insert order num in adress for online  Colissimo.

= 1.2.2 (2016-02-01) =
* Add of Custom mode.
* Minor tweaks.

= 1.1.3 (2016-01-16) =
* Fix in HStariff code iMacros script.

= 1.1.2 (2016-01-16) =
* Fields added in csv manual gateway.

= 1.1.1 (2016-01-14) =
* Minor changes.

= 1.1.0 (2016-01-13) =
* Setting added for auto clean Colissimo orders.
* Check orders number in gateway 
* Add billing phone and billing email in gateway output.
* Imacros script as gateway output for online browser.
* Auto calculate article CN23 fields

= 1.0.5 (2016-01-05) =
* Automatic delete of Colissimo orders in gateway when tracking code known.
* Colissimo status redefined

= 1.0.4 (2016-01-06) =
* Minor changes for svn install.

= 1.0.2 (2016-01-03) =
* Initial release in Wordpress
