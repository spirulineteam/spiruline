<?php
/**
 * La configuration de base de votre installation WordPress.
 *
 * Ce fichier contient les réglages de configuration suivants : réglages MySQL,
 * préfixe de table, clés secrètes, langue utilisée, et ABSPATH.
 * Vous pouvez en savoir plus à leur sujet en allant sur
 * {@link http://codex.wordpress.org/fr:Modifier_wp-config.php Modifier
 * wp-config.php}. C’est votre hébergeur qui doit vous donner vos
 * codes MySQL.
 *
 * Ce fichier est utilisé par le script de création de wp-config.php pendant
 * le processus d’installation. Vous n’avez pas à utiliser le site web, vous
 * pouvez simplement renommer ce fichier en "wp-config.php" et remplir les
 * valeurs.
 *
 * @package WordPress
 */

 define('WP_ALLOW_REPAIR', true);
 
// ** Réglages MySQL - Votre hébergeur doit vous fournir ces informations. ** //
/** Nom de la base de données de WordPress. */
define('DB_NAME', 'spiruline');

/** Utilisateur de la base de données MySQL. */
define('DB_USER', 'root');

/** Mot de passe de la base de données MySQL. */
define('DB_PASSWORD', 'root');

/** Adresse de l’hébergement MySQL. */
define('DB_HOST', '163.172.63.91');

/** Jeu de caractères à utiliser par la base de données lors de la création des tables. */
define('DB_CHARSET', 'utf8mb4');

/** Type de collation de la base de données.
  * N’y touchez que si vous savez ce que vous faites.
  */
define('DB_COLLATE', '');

/**#@+
 * Clés uniques d’authentification et salage.
 *
 * Remplacez les valeurs par défaut par des phrases uniques !
 * Vous pouvez générer des phrases aléatoires en utilisant
 * {@link https://api.wordpress.org/secret-key/1.1/salt/ le service de clefs secrètes de WordPress.org}.
 * Vous pouvez modifier ces phrases à n’importe quel moment, afin d’invalider tous les cookies existants.
 * Cela forcera également tous les utilisateurs à se reconnecter.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '}L_{-(df_pF:$2r}Lo}pdwN`hpeSft[}?jH2os,4ITc sYj01&o$+rE/,tR;4~Ud');
define('SECURE_AUTH_KEY',  '-Tp&S0G3IxprN&K,b3AH9XltEH3:rvRf>)w7Hd?Iu{`T.1k^!J23nxylirob]J_L');
define('LOGGED_IN_KEY',    '0mOys?jYC={BEBJY>sA fuB)Y,2_y;a6Iw01 ;Q5dynp>ZD!)Xqhp%y.RFsX<y_J');
define('NONCE_KEY',        'yM{vWG~;j`^H5@nnP:/H?E>%~87ca>jqWHlo/YCw;Ygm6l9y%F%?inQ:4(LJsTs2');
define('AUTH_SALT',        'b^RbByvMKh3s@3@M D!;8Cc }g`!Tzd6iSp$Jx2k|VkP:H)5bufbo849, 1~30#l');
define('SECURE_AUTH_SALT', '1TWH)_*GN!.,LIa7Ib/^MG>cUiu7bKB45ZMV4Kw?x=s,hKt0=?Qmz2}3C;/+Hu!:');
define('LOGGED_IN_SALT',   'KS~Js2;[Bh) /<;uN6sM].Mf1FN*.jGXHNBw hR*::W 1Bf,kk^x>lg5>?MN^?vs');
define('NONCE_SALT',       '@XnDhG$Zw{~;a::Za,R50Y`;AIa=>?vbhKuR8 ZYQ:z_2<{ b^%uY-0lxYRjm_=$');
/**#@-*/

/**
 * Préfixe de base de données pour les tables de WordPress.
 *
 * Vous pouvez installer plusieurs WordPress sur une seule base de données
 * si vous leur donnez chacune un préfixe unique.
 * N’utilisez que des chiffres, des lettres non-accentuées, et des caractères soulignés !
 */
$table_prefix  = 'wp_';

/**
 * Pour les développeurs : le mode déboguage de WordPress.
 *
 * En passant la valeur suivante à "true", vous activez l’affichage des
 * notifications d’erreurs pendant vos essais.
 * Il est fortemment recommandé que les développeurs d’extensions et
 * de thèmes se servent de WP_DEBUG dans leur environnement de
 * développement.
 *
 * Pour plus d’information sur les autres constantes qui peuvent être utilisées
 * pour le déboguage, rendez-vous sur le Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* C’est tout, ne touchez pas à ce qui suit ! */

/** Chemin absolu vers le dossier de WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Réglage des variables de WordPress et de ses fichiers inclus. */
require_once(ABSPATH . 'wp-settings.php');